import fnmatch
import os
import pathlib
import shutil
import tempfile

import msgpack
import pytest
from pytest_idem.runner import run_sls


async def test_failed_enter(hub):
    """
    Verify that exceptions raised in an "enter" function completely halt the program
    """
    context = hub.idem.managed.context(
        run_name="test", cache_dir=tempfile.gettempdir(), esm_plugin="fail"
    )

    with pytest.raises(RuntimeError) as e:
        async with context:
            ...
        assert "Fail to enter enforced state management" in str(e)


def test_arg_bind_ref_from_esm():
    """
    Test that you can do argument binding using ESM if referenced value is not found in current run
    """
    id_ = "test_|-esm_test_state_|-esm_test_state_|-"
    present_tag = f"{id_}present"
    expected_data = {id_: {"key": "value", "name": "esm_test_state"}}

    cache_dir = tempfile.mkdtemp()
    cache_file_dir = pathlib.Path(cache_dir) / "esm" / "cache"
    cache_file_pattern = "test-*.msgpack"
    local_plugin_cache = pathlib.Path(cache_dir) / "esm" / "local" / "test.msgpack"

    try:
        # Run the test so that a cache exists
        # We run this file to prepare cache and argument bind values will be referred from this cache in other files
        ret = run_sls(["esm.arg_bind_esm"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is True
        assert ret[present_tag]["old_state"] is None
        assert ret[present_tag]["new_state"] == {
            "key": "value",
            "name": "esm_test_state",
        }
        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        id_ = "test_|-esm_test_state_ref_|-esm_test_state_ref_|-"
        present_tag = f"{id_}present"
        expected_data.update({id_: {"key1": "value", "name_ref": "esm_test_state"}})

        # We run this file individually, this file refers values previously run file
        # Making sure that argument binding is resolved
        # even when the file from which values are referred is not being run in this run.
        ret1 = run_sls(["esm.arg_bind_esm_ref"], cache_dir=cache_dir)
        assert present_tag in ret1
        assert ret1[present_tag]["result"] is True
        assert ret1[present_tag]["old_state"] is None
        assert ret1[present_tag]["new_state"] == {
            "key1": "value",
            "name_ref": "esm_test_state",
        }

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        # checking for failure case
        # if the argument bind value is not present in ESM then it should throw error
        fail_present_tag = (
            "test_|-esm_test_state_ref_fail_|-esm_test_state_ref_fail_|-present"
        )
        assert ret1[fail_present_tag]["result"] is False
        assert ret1[fail_present_tag]["old_state"] is None
        assert ret1[fail_present_tag]["new_state"] is None
        assert (
            ret1[fail_present_tag]["comment"]
            == "Requisite arg_bind test:esm_test_state_fail not found"
        )

        # checking if the referenced state is present in current run and ESM
        # Argument binding should use value from  current run
        ret1 = run_sls(["esm.arg_bind_esm_ref_update"], cache_dir=cache_dir)
        assert present_tag in ret1
        assert ret1[present_tag]["result"] is True
        assert ret1[present_tag]["old_state"] == {
            "key1": "value",
            "name_ref": "esm_test_state",
        }
        assert ret1[present_tag]["new_state"] == {
            "key1": "value_updated",
            "name_ref": "esm_test_state_updated",
        }

    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)


def test_present():
    """
    Verify that a failed `present` function doesn't overwrite previous esm data
    """
    id_ = "test_|-esm_test_state_|-esm_test_state_|-"
    present_tag = f"{id_}present"
    absent_tag = f"{id_}absent"
    expected_data = {id_: {"key": "value"}}

    cache_dir = tempfile.mkdtemp()
    cache_file_dir = pathlib.Path(cache_dir) / "esm" / "cache"
    cache_file_pattern = "test-*.msgpack"
    local_plugin_cache = pathlib.Path(cache_dir) / "esm" / "local" / "test.msgpack"

    try:
        # Run the test so that a cache exists
        ret = run_sls(["esm.pass"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is True
        assert ret[present_tag]["old_state"] is None
        assert ret[present_tag]["new_state"] == {"key": "value"}

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        # Verify that new_state ends up in ctx as old_state
        ret = run_sls(["esm.pass"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is True
        assert ret[present_tag]["old_state"] == {"key": "value"}
        assert ret[present_tag]["new_state"] == {"key": "value"}

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        # After a state fails and returns no data, the old_state should still be there
        ret = run_sls(["esm.fail"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is False
        assert ret[present_tag]["new_state"] is None
        assert ret[present_tag]["old_state"] == {"key": "value"}

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        # Check after a second failure just to be sure
        ret = run_sls(["esm.fail"], cache_dir=cache_dir)
        assert present_tag in ret
        assert ret[present_tag]["result"] is False
        assert ret[present_tag]["new_state"] is None
        assert ret[present_tag]["old_state"] == {"key": "value"}

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file, expected_data)
        _verify_cache(local_plugin_cache, expected_data)
        cache_file.unlink()

        # Run the absent state, old_state should appear in the return, but not in the cache
        ret = run_sls(["esm.clean"], cache_dir=cache_dir)
        assert absent_tag in ret
        assert ret[absent_tag]["result"] is True
        assert ret[absent_tag]["new_state"] is None
        assert ret[absent_tag]["old_state"] == {"key": "value"}

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file)
        _verify_cache(local_plugin_cache)
        cache_file.unlink()

        # The next time absent is run, old_state is cleared
        ret = run_sls(["esm.clean"], cache_dir=cache_dir)
        assert absent_tag in ret
        assert ret[absent_tag]["result"] is True
        assert ret[absent_tag]["new_state"] is None
        assert ret[absent_tag]["old_state"] == None

        cache_file = _find_file(cache_file_dir, cache_file_pattern)
        _verify_cache(cache_file)
        _verify_cache(local_plugin_cache)
        cache_file.unlink()

    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)


def test_exec():
    """
    Verify that exec result is not persisted/cached,
    since the result may be a list of resources.
    """
    id_ = "exec_|-dont_persist_exec_|-dont_persist_exec_|-"
    run_tag = f"{id_}run"

    cache_dir = tempfile.mkdtemp()
    cache_file = pathlib.Path(cache_dir) / "esm" / "cache" / "test.msgpack"
    local_plugin_cache = pathlib.Path(cache_dir) / "esm" / "local" / "test.msgpack"

    try:
        # Run the test so that a cache exists
        ret = run_sls(["esm.exec"], cache_dir=cache_dir)
        assert run_tag in ret
        assert ret[run_tag]["result"] is True
        assert ret[run_tag]["old_state"] is None
        assert ret[run_tag]["new_state"] is True
        assert ret[run_tag]["changes"] is not None

        assert cache_file.exists() is False, f"No cache file expected: {cache_file}"
        _verify_cache(local_plugin_cache)
    finally:
        shutil.rmtree(cache_dir, ignore_errors=True)


def _verify_cache(cache_file: pathlib.Path, expected_data=None):
    assert cache_file.exists()
    with cache_file.open("rb") as fh:
        data = msgpack.load(fh)

    if expected_data is None:
        assert not data
        return

    assert data == expected_data


def _find_file(dir_path: pathlib.Path, pattern: str) -> pathlib.Path:
    """
    Find a file within a directory according to the file name pattern.
    :param dir_path: directory path under which the search happens
    :param pattern: file name pattern
    """
    for root, dirs, files in os.walk(top=dir_path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return dir_path / name
