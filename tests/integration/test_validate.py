import json
import subprocess
import sys

from pytest_idem.runner import run_sls
from pytest_idem.runner import run_sls_validate


def assert_params(params):
    assert params
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""
    assert params["rg_name"]["your_rg"] == "default"
    assert params["locations"]
    assert isinstance(params["locations"], list)
    assert len(params["locations"]) == 5
    assert isinstance(params["locations"][4]["state"], dict)
    assert params["locations"][4]["state"]["city"] == ""


def test_validate_params(hub):
    ret = run_sls_validate(["success"])
    assert ret
    params = ret["params"].params()
    assert_params(params)

    parameters = ret["parameters"]
    assert_params(parameters["GLOBAL"])
    assert parameters["ID_DECS"]
    parameters_state = parameters["ID_DECS"]
    assert "sls_refs" in ret
    sls_ref = next(iter(ret["sls_refs"].keys()))
    assert (
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    assert (
        sls_ref + ".allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    params = parameters_state[
        sls_ref + ".allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"{sls_ref}.{{{{ params.get('{key}') }}}}"
        assert state_id in parameters_state
        params = parameters_state[state_id]
        assert key in params
        assert isinstance(params[key], str)
        assert params[key] == ""

    warnings = ret["warnings"]
    assert warnings
    assert warnings["ID_DECS"]
    assert warnings["ID_DECS"][
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    id_warnings = warnings["ID_DECS"][
        sls_ref
        + ".Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert id_warnings["params_meta_missing"]
    assert "rg_name.your_rg" in id_warnings["params_meta_missing"]
    assert "locations" in id_warnings["params_meta_missing"]
    assert "locations[].state" in id_warnings["params_meta_missing"]

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"{sls_ref}.{{{{ params.get('{key}') }}}}"
        assert warnings["ID_DECS"][state_id]
        id_warnings = warnings["ID_DECS"][state_id]
        assert "params_meta_missing" in id_warnings
        assert key in id_warnings["params_meta_missing"]


def test_validate_policy_params(hub):
    ret = run_sls_validate(["policy"])
    assert ret
    assert ret["parameters"]
    assert ret["parameters"]["GLOBAL"]
    assert "tag_key_value" in ret["parameters"]["GLOBAL"]
    assert ret["parameters"]["ID_DECS"]
    for _, value in ret["parameters"]["ID_DECS"].items():
        assert "organization_unit_id" not in value.keys()
        assert "tag_key_value" in value.keys()


def test_invalid_syntax(hub):
    # Verify a descriptive message is thrown with invalid syntax
    try:
        run_sls(["invalid_syntax"])
    except Exception as e:
        assert "Invalid syntax" in e.args[0]


def test_validate_cli(runpy, tests_dir, mock_time):
    cmd = [
        sys.executable,
        runpy,
        "validate",
        tests_dir / "sls" / "target.sls",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    output = json.loads(ret.stdout)

    assert 5 == len(output), len(output)
