from typing import Any
from typing import Dict

__func_alias__ = {"list_": "list"}
__contracts__ = ["auto_state", "soft_fail"]


def __init__(hub):
    hub.exec.tests.auto.ITEMS = {}
    hub.exec.tests.auto.ACCT = ["test"]


def get(hub, ctx, name: str, **kwargs) -> Dict[str, Any]:
    result = dict(comment="", result=True, ret=None)
    result["ret"] = hub.exec.tests.auto.ITEMS[name]
    return result


def list_(hub, ctx) -> Dict[str, Any]:
    return dict(comment="", result=True, ret=hub.exec.tests.auto.ITEMS)


def create(hub, ctx, name: str, kw1=None, **kwargs) -> Dict[str, Any]:
    result = dict(comment="", result=True, ret=None)
    hub.exec.tests.auto.ITEMS[name] = dict(kw1=kw1, **kwargs)
    result["ret"] = hub.exec.tests.auto.ITEMS[name]
    result["comment"] = f"Created '{name}'"
    return result


def update(hub, ctx, name: str, **kwargs) -> Dict[str, Any]:
    result = dict(comment="", result=True, ret=None)
    d = hub.exec.tests.auto.ITEMS[name]
    d.update(kwargs)
    hub.exec.tests.auto.ITEMS[name] = d

    result["comment"] = f"Updated '{name}'"
    return result


def delete(hub, ctx, name: str, **kwargs) -> Dict[str, Any]:
    result = dict(comment="", result=True, ret=None)
    result["ret"] = hub.exec.tests.auto.ITEMS.pop(name)
    result["comment"] = f"Deleted '{name}'"
    return result
