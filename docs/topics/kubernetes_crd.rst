======================
Kubernetes CRD support
======================
Idem supports using `Kubernetes CRD <https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/>`_ to execute state. The kubernetes CRD is internally converted into SLS format used by idem.

CRD format
----------
The CRD format is similar to kubernetes syntax. Following is a sample CRD SLS file:

.. code-block:: yaml

    apiVersion: resource-management.azure.idem.vmware.com/v1alpha1
    kind: resource-groups
    metadata:
      name: new-rg
    spec:
      - resource_group_name: new-rg
      - parameters:
          location: eastus
          tags:
            env: new-rg
            Unit: CMBU

The above CRD gets converted internally into the following SLS:

.. code-block:: sls

    new-rg:
      azure.resource_management.resource_groups.present:
      - resource_group_name: new-rg
      - parameters:
        location: eastus
        tags:
          env: new-rg
          Unit: CMBU

As clear from the above example:

1. ``apiVersion`` together with ``kind`` attribute identifies the `path reference` of the SLS.
2. ``metadata``'s ``name`` is the state id of the SLS.
3. ``spec`` contains any parameters required by the resulting state.
4. `function reference` is always `present` by default, but can be inverted using command-line parameter ``--invert``.

Execution
---------

To execute a Kubernetes CRD, command-line parameter ``--render 'jinja|yaml|k8crd'`` needs to be specified.

Following example details a `resource group` creation using CRD as mentioned in previous section:

.. raw:: html

    <details>
    <summary>Click here to see actual execution</summary>

.. code-block::

    $ idem state --output json --render 'jinja|yaml|k8crd' crd.sls
    {
        "azure.resource_management.resource_groups_|-new-rg_|-new-rg_|-present": {
            "changes": {
                "new": {
                    "id": "/subscriptions/subscription-id/resourceGroups/new-rg",
                    "name": "new-rg",
                    "type": "Microsoft.Resources/resourceGroups",
                    "location": "eastus",
                    "tags": {
                        "env": "new-rg",
                        "Unit": "CMBU"
                    },
                    "properties": {
                        "provisioningState": "Succeeded"
                    }
                }
            },
            "comment": "Created",
            "name": "new-rg",
            "result": true,
            "old_state": null,
            "new_state": null,
            "__run_num": 1
        }
    }

.. raw:: html

    </details>

Following example details `resouce group` deletion using the same CRD:

.. raw:: html

    <details>
    <summary>Click here to see actual execution</summary>

.. code-block::

    $ idem state --output json --render 'jinja|yaml|k8crd' --invert crd.sls
    {
        "azure.resource_management.resource_groups_|-new-rg_|-new-rg_|-absent": {
            "changes": {
                "old": {
                    "id": "/subscriptions/subscription-id/resourceGroups/new-rg",
                    "name": "new-rg",
                    "type": "Microsoft.Resources/resourceGroups",
                    "location": "eastus",
                    "tags": {
                        "env": "new-rg",
                        "Unit": "CMBU"
                    },
                    "properties": {
                        "provisioningState": "Succeeded"
                    }
                }
            },
            "comment": "Accepted",
            "name": "new-rg",
            "result": true,
            "old_state": null,
            "new_state": null,
            "__run_num": 1
        }
    }

.. raw:: html

    </details>
