.. idem documentation master file, created by
   sphinx-quickstart on Wed Feb 20 15:36:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem's documentation!
================================

.. toctree::
   :maxdepth: 3
   :glob:

   topics/intro
   topics/config
   topics/doc
   topics/extending
   topics/add_requisites
   topics/sls_meta
   topics/sls_structure
   topics/parameters
   topics/params_validation
   topics/argument_binding
   topics/sls_inversion
   topics/jmespath
   topics/transparent_req
   topics/acct
   topics/acct_file
   topics/ignore_changes_requisite
   topics/jinja_arg_bind_delayed_rend
   topics/delayed_rendering
   topics/sensitive_requisite
   topics/sls_acct
   topics/sls_sources
   topics/sls_tree
   topics/sls_exec
   topics/sls_resolver_plugins
   topics/group_plugins
   topics/reconciliation_loop
   topics/enforced_state_management
   topics/count
   topics/events
   topics/progress
   topics/kubernetes_crd
   topics/scripts
   topics/describe
   topics/helper_functions/index
   topics/single_target
   tutorial/index

   topics/azure_docs
   topics/migrate_salt
   releases/index

.. toctree::
   :caption: Get involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
