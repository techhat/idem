#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.9/tests.txt requirements/tests.in
#
-e file:.#egg=idem
    # via
    #   -r requirements/tests.in
    #   pytest-idem
acct==8.3.0
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-evbus
aio-pika==8.2.1
    # via evbus-pika
aiofiles==22.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
aiokafka==0.7.2
    # via evbus-kafka
aiormq==6.4.2
    # via aio-pika
altgraph==0.17.2
    # via pyinstaller
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==22.1.0
    # via pytest
cffi==1.15.1
    # via cryptography
colorama==0.4.5
    # via
    #   -r requirements/base.txt
    #   -r requirements/tests.in
    #   idem
    #   rend
cryptography==38.0.1
    # via acct
dict-toolbox==2.3.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
evbus-kafka==4.0.1
    # via -r requirements/extra/kafka.txt
evbus-pika==3.0.1
    # via -r requirements/extra/rabbitmq.txt
idna==3.4
    # via yarl
iniconfig==1.1.1
    # via pytest
jinja2==3.1.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   rend
jmespath==1.0.1
    # via
    #   -r requirements/base.txt
    #   idem
kafka-python==2.0.2
    # via aiokafka
lazy-object-proxy==1.7.1
    # via pop
markupsafe==2.1.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.4
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
multidict==6.0.2
    # via yarl
nest-asyncio==1.5.5
    # via
    #   pop-loop
    #   pytest-pop
packaging==21.3
    # via pytest
pamqp==3.2.1
    # via aiormq
pluggy==1.0.0
    # via pytest
pop-config==10.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.1.3
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
pop-loop==1.0.5
    # via
    #   -r requirements/base.txt
    #   idem
    #   pop
pop-serial==1.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-evbus
pop-tree==9.3.0
    # via
    #   -r requirements/base.txt
    #   idem
pop==22.0.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
py==1.11.0
    # via pytest
pycparser==2.21
    # via cffi
pyinstaller-hooks-contrib==2022.10
    # via pyinstaller
pyinstaller==5.0.1
    # via
    #   -r requirements/package.in
    #   tiamat-pip
pyparsing==3.0.9
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via pytest-pop
pytest-idem==3.1.0
    # via -r requirements/tests.in
pytest-pop==10.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest==7.1.3
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.4.6
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
sniffio==1.3.0
    # via pop-loop
tiamat-pip==1.5.1
    # via -r requirements/package.in
toml==0.10.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   rend
tomli==2.0.1
    # via pytest
tqdm==4.64.1
    # via
    #   -r requirements/base.txt
    #   idem
wheel==0.37.1
    # via
    #   -r requirements/base.txt
    #   idem
yarl==1.8.1
    # via
    #   aio-pika
    #   aiormq

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
